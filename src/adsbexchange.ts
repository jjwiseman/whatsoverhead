import axios, { AxiosRequestConfig } from "axios"

export type AdsBExchangeOptions = {
    apiKey: string;
    url?: string;
}

export type Coords = {
    lat: number;
    lon: number;
}

const ADSBEXCHANGE_API_URL = "https://adsbexchange.com/api/aircraft/json/";

export type Aircraft = {
    icao: string,
    reg: string,
    lat: number,
    lon: number,
    alt: number,
    heading?: number,
    callsign: string,
}

export class AdsBExchange {
    private apiKey: string;
    private url: string;

    constructor(options: AdsBExchangeOptions) {
        this.apiKey = options.apiKey;
        this.url = options.url || ADSBEXCHANGE_API_URL;
        // axios.interceptors.request.use(request => {
        //     console.log('Starting Request', request)
        //     return request
        //   });
    }

    parseAircraft(ac: any): Aircraft | null {
        if (ac.lat === 0 || ac.lon === 0) {
            return null;
        } else {
            return {
                icao: ac.icao,
                lat: parseFloat(ac.lat),
                lon: parseFloat(ac.lon),
                reg: ac.reg,
                alt: parseFloat(ac.alt),
                heading: parseFloat(ac.trak),
                callsign: ac.call
            };
        }
    }

    async getAircraft(loc: Coords, radiusNm: number): Promise<Aircraft[]> {
        const url = this.url + `lat/${loc.lat}/lon/${loc.lon}/dist/${radiusNm}/`;
        const options: AxiosRequestConfig = {
            headers: { "api-auth": this.apiKey }
        };
        const result = await axios.get(url, options)
        const rawAircraft = result.data.ac;
        if (rawAircraft) {
            const result = rawAircraft.map(this.parseAircraft);
            console.log("ADSBx parsed result:", result);
            return result;
        } else {
            return [];
        }
    }

}