
import express, { Request, Response } from "express";
import http from "http";
import { getDistance, getRhumbLineBearing } from "geolib";
import fs from "fs";
import sqlite from "sqlite";
import sqlite3 from "sqlite3";

import { AdsBExchange, AdsBExchangeOptions, Aircraft, Coords } from "./adsbexchange";

const expressAsync = (fn: Function) => (...args: any[]) => fn(...args).catch(args[2]);


function parseLoc(locStr: string): Coords {
    const latLonRe = /^(-?\d+(?:\.\d+)?),(-?\d+(?:\.\d+)?)$/;
    const result = latLonRe.exec(locStr);
    if (!result || result.length != 3) {
        throw Error(`Could not parse loc: ${locStr}`);
    }
    return { lat: parseFloat(result[1]), lon: parseFloat(result[2]) };
}


// An Aircraft, but relative to an observer.
type SituatedAircraft = Aircraft & {
    range: number,
    bearing: number
};

function situateAircraft(loc: Coords, aircraft: Aircraft): SituatedAircraft {
    const sitAc = aircraft as SituatedAircraft;
    sitAc.range = getDistance(loc, aircraft);
    sitAc.bearing = getRhumbLineBearing(loc, aircraft);
    return sitAc;
}


function describeAircraft(ac: SituatedAircraft, basestationInfo: BasestationRecord | null): string {
    const rangeMiles = (ac.range * 0.000621371).toFixed(1);
    const bearing = ac.bearing.toFixed(0);
    var text = "";
    if (ac.reg) {
        text += `${ac.reg},`;
    } else if (basestationInfo && basestationInfo.reg && basestationInfo.reg !== "") {
        text += `${basestationInfo.reg},`;
    } else {
        text += `ICAO ${ac.icao},`;
    }
    if (basestationInfo && basestationInfo.type && basestationInfo.type !== "") {
        text += ` a ${basestationInfo.type},`
    }
    if (basestationInfo && basestationInfo.owner && basestationInfo.owner !== "") {
        text += ` registered to ${basestationInfo.owner},`;
    }
    text += ` is ${rangeMiles} miles away at bearing ${bearing} degrees`;
    if (ac.alt) {
        text += `, altitude ${ac.alt}`;
    }
    return text;
}

// Info on an aircraft from basestation.sqb.

type BasestationRecord = {
    reg: string,
    type: string,
    owner: string
};

class AppServer {
    app: express.Application;
    static PORT = 3000;
    adsbx: AdsBExchange;
    db?: sqlite.Database;

    constructor() {
        this.app = express();
        const apiKey = process.env.ADSBX_API_KEY;
        if (!apiKey) {
            throw Error("Must specify API_KEY");
        }
        this.adsbx = new AdsBExchange({ apiKey });
    }

    async init() {
        this.routes();
        this.errors();
        console.log("Connecting to database");
        this.db = await sqlite.open(
            "clean-basestation.sqb",
            { mode: sqlite3.OPEN_READONLY, promise: Promise });
        console.log("Ready");
    }

    static async start() {
        const appServer = new AppServer();
        await appServer.init();
        const server = http.createServer(appServer.app);
        server.listen(
            AppServer.PORT,
            "0.0.0.0",
            () => console.log(`Listening on port ${AppServer.PORT}`));
    }

    private routes() {
        this.app.get("/whatsoverhead", expressAsync(async (req: Request, res: Response) => {
            const locStr: string = req.query.loc;
            if (!locStr) {
                throw Error("Must specify loc parameter");
            }
            const loc: Coords = parseLoc(locStr);
            console.log(loc);
            const planes = await this.adsbx.getAircraft(loc, 6);
            if (!planes || planes.length == 0) {
                res.status(200).send("I don't see anything flying near you, sorry.");
            } else {
                const situatedPlanes = planes.map(ac => situateAircraft(loc, ac));
                situatedPlanes.sort((a1: any, a2: any) => a1.range - a2.range);
                const closestPlane = situatedPlanes[0];
                const basestationInfo = await this.getBasestationInfo(closestPlane.icao);
                const text = describeAircraft(closestPlane, basestationInfo);
                console.log(text);
                res.status(200).send(text);
            }
        }));
    }

    async getBasestationInfo(icao: string): Promise<BasestationRecord | null> {
        if (!this.db) {
            throw Error("Database not connected");
        }
        const record = await this.db.get(
            "SELECT Registration, Type, RegisteredOwners FROM Aircraft WHERE ModeS = ?",
            icao);
        console.log(record);
        if (record) {
            return {
                reg: record["Registration"],
                type: record["Type"],
                owner: record["RegisteredOwners"]
            };
        } else {
            return null;
        }
    }

    private errors() {
        this.app.use((err: Error, req: Request, res: Response, next: Function) => {
            console.error(err);
            res.status(500).send("Sorry, unable.");
        });
    }

    private logs() {
        this.app.use((req, res) => {
            console.log('REQUEST IS: ', req.path, new Date().toLocaleString());
        })
    }
}
AppServer.start();
